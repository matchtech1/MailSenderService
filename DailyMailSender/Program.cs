﻿using DailyMailSender;
using MailSenderManagement;
/*
 Windows Task Scheduler to run this code every day at the specified hour.
*/

const string MAIL_SENDER_RUN_TIME = "10:00";
var mailMarketingManager = new MailMarketingManager();

string currentTime = DateTime.Now.ToString("HH:mm");

if (currentTime == MAIL_SENDER_RUN_TIME)
{
    mailMarketingManager.SendMarketingMessages();
}