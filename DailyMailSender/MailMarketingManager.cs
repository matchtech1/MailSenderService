using MailSenderManagement;
using MailSenderManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyMailSender
{
    public class MailMarketingManager
    {
        const string SERVER_ADDRESS = "matchtech2@gmail.com";
        const string PASSWORD = "#password#";
        private readonly MailSender _mailSender;
        private readonly DBHandler _dbHander;

        public MailMarketingManager()
        {
            _dbHander = DBHandler.GetInstance();
            _mailSender = MailSender.GetInstance(SERVER_ADDRESS, PASSWORD);
        }


        public void SendMarketingMessages()
        {
            var users = _dbHander.GetAllUsers();
            foreach (var user in users)
            {
                var mailInfo = (user.IsRecuiter) ? 
                    GetCanidateMailInfo(user.FullName): 
                    GetRecuiterMailInfo(user.FullName);
                _mailSender.SendEmail(user.Email, mailInfo);
            }
        }

        private static MailInfo GetCanidateMailInfo(string fullName)
        {
            return new MailInfo()
            {
                Subject = "Your Personalized Job Matches",
                Body = $"Dear {fullName},\r\n\r\nAs a valued customer of our job matching system, " +
                $"you already know the benefits of our service. Log in to your personal profile today to discover new job " +
                $"matches personalized to your skills and preferences.\n\n" +
                $"Best regards,\r\nMatchTech team\r\nhttps://www.matchtech.com/",
            };
        }
        private static MailInfo GetRecuiterMailInfo(string fullName)
        {
            return new MailInfo()
            {
                Subject = "Simplify Your Recruitment Process",
                Body = $"Dear {fullName},\n\n" +
                "Our job matching system connects you with top talent easily by matching you with candidates " +
                "whose skills and preferences match your job postings. Simply log in to our system and " +
                "browse through the CVs of highly suited candidates.\n\nStreamline your recruitment process" +
                " and find the perfect candidate for your company today.\n\n" +
                "Best regards,\r\nMatchTech team\r\nhttps://www.matchtech.com/"
            };
        }
    }
}
