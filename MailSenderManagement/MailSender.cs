﻿using System.Net.Mail;
using System.Net;
using MailSenderManagement.Models;

namespace MailSenderManagement
{
    public class MailSender
    {
        private string _serverMail;
        private string _serverPassword;
        public static MailSender _instance;
        public DBHandler _dBHandler;
        private const string RESUME_DIR = @"C:\MatchTech\Resume";

        private MailSender(string serverMail, string serverPassword)
        {
            this._serverMail = serverMail;
            this._serverPassword = serverPassword;
            this._dBHandler = DBHandler.GetInstance();
        }

        public static MailSender GetInstance(string serverMail, string serverPassword)
        {
            return _instance ??= new MailSender(serverMail, serverPassword);
        }

        public async Task SendResume(string authToken, string positionId)
        {
            var toEmail = await _dBHandler.GetMailByPositionId(positionId);
            var userId = await _dBHandler.GetUserIdByAuth(authToken);

            string positionName = (await _dBHandler.GetPosition(positionId)).PositionName;
            string recruiterFullName = (await _dBHandler.GetUser(userId)).FullName;

            var mailInfo = new MailInfo()
            {
                Subject = $"Application for {positionName} position",
                Body = $"Dear {recruiterFullName},\r\n\r\nWe are writing to recommend a candidate for the {positionName}" +
                $" position that was recently advertised on your company's website. Based on the job description and the " +
                $"candidate's qualifications, we believe that they would be an excellent fit for your team.\r\n\r\nWe " +
                $"have attached the candidate's resume to this email for your consideration.\r\n\r\nWe believe that " +
                $"the candidate's qualifications and experience make them an ideal candidate for the {positionName} " +
                $"position.\r\n\r\nThank you for considering our recommendation. We look forward to the opportunity " +
                $"to discuss the candidate's qualifications further.\r\n\r\nSincerely,\r\n\r\nThe MatchTech System",
                Attachment = Path.Combine(RESUME_DIR, $"{userId}.pdf")
            };

            SendEmail(toEmail, mailInfo);
        }

        public void SendEmail(string toEmail, MailInfo mailInfo)
        {
            // configure SMTP client
            var smtpClient = new SmtpClient("smtp.gmail.com", 587)
            {
                EnableSsl = true,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(_serverMail, _serverPassword)
            };

            // create message
            var message = new MailMessage()
            {
                Subject = mailInfo.Subject,
                Body = mailInfo.Body,
                From = new MailAddress(_serverMail),
            };
            if (!string.IsNullOrEmpty(mailInfo.Attachment))
            {
                var attach = new Attachment(mailInfo.Attachment);
                message.Attachments.Add(attach);
            }
            message.To.Add(new MailAddress(toEmail));

            smtpClient.Send(message);
        }
    }
}
