﻿using System.Net.Mail;
using System.Net;
using MongoDB.Driver;
using MailSenderManagement.Models;

namespace MailSenderManagement
{
    public class DBHandler
    {
        private const string RESUME_DIR = @"C:\MatchTech\Resume";
        public static DBHandler _instance;

        private MongoClient _mongoClient { get; set; }
        public readonly IMongoDatabase MongoDatabase;

        private readonly IMongoCollection<UserModel> _usersCollection;
        private readonly IMongoCollection<PositionModel> _positionsCollection;
        private const string DB_NAME = "MatchTech";
        private const string USERS_COLLECTION_NAME = "Users";
        private const string POSITIONS_COLLECTION_NAME = "Positions";

        private DBHandler()
        {
            string MongoClientConnectionString = "mongodb://127.0.0.1:27017";
            var mongoClient = new MongoClient(MongoClientConnectionString);
            var mongoDatabase = mongoClient.GetDatabase(DB_NAME);

            _usersCollection = mongoDatabase.GetCollection<UserModel>(USERS_COLLECTION_NAME);
            _positionsCollection = mongoDatabase.GetCollection<PositionModel>(POSITIONS_COLLECTION_NAME);
        }

        public async Task<string> GetUserIdByAuth(string authToken)
        {
            var filter = Builders<UserModel>.Filter.Eq(user => user.AuthToken, authToken);
            var user = await _usersCollection.Find(filter).SingleOrDefaultAsync();
            if (user == null)
            {
                return string.Empty;
            }
            return user.Id;
        }

        public async Task<string> GetMailByPositionId(string positionId)
        {
            var position = await GetPosition(positionId);
            var recuiterFilter = Builders<UserModel>.Filter.Eq(user => user.Id, position.RecuiterId);
            var recuiter = await _usersCollection.Find(recuiterFilter).SingleOrDefaultAsync();
            if (recuiter == null)
            {
                return string.Empty;
            }
            return recuiter.Email;
        }

        public async Task<UserModel> GetUser(string userId)
        {
            var userFilter = Builders<UserModel>.Filter.Eq(user => user.Id, userId);
            return await _usersCollection.Find(userFilter).SingleOrDefaultAsync();
        }

        public IEnumerable<UserModel> GetAllUsers()
        {
            var filter = Builders<UserModel>.Filter.Empty;
            return _usersCollection.Find(filter).ToEnumerable();
        }

        public async Task<PositionModel> GetPosition(string positionId)
        {
            var positionFilter = Builders<PositionModel>.Filter.Eq(position => position.Id, positionId);
            return await _positionsCollection.Find(positionFilter).SingleOrDefaultAsync();
        }

        public static DBHandler GetInstance()
        {
            return _instance ??= new DBHandler();
        }
    }
}
