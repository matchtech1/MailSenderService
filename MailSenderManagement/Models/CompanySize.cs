﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MailSenderManagement.Models
{
    public class CompanySize
    {
        public int From { get; set; }
        public int To { get; set; }
    }
}
