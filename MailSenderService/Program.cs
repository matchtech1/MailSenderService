using MailSenderManagement;
using MailSenderService;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddCors(options => options.AddPolicy("AlloweAcess_To_Api",
    policy => policy
    .AllowAnyOrigin()
    .AllowAnyMethod()
    .AllowAnyHeader()
));

//mail sender config
var mailConfig = builder.Configuration.GetSection("Mail").Get<MailConfiguration>();
var mailSender = MailSender.GetInstance(mailConfig.MailAddress, mailConfig.Password);
builder.Services.AddSingleton(mailSender);

var app = builder.Build();
app.UseCors("AlloweAcess_To_Api");
app.UseRouting();

// Configure the HTTP request pipeline.
app.UseCors(builder => builder
    .AllowAnyOrigin()
    .AllowAnyMethod()
    .AllowAnyHeader());
app.UseHttpsRedirection();
app.UseAuthorization();

app.MapControllers();

app.Run();
