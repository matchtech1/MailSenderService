﻿using MailSenderManagement;
using MailSenderService.Dto;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace MailSenderService.Controllers
{
    [EnableCors("AllowSpecificOrigin")]
    [ApiController]
    [Route("[controller]")]
    public class MailSenderController : Controller
    {
        private readonly ILogger<MailSenderController> _logger;
        private readonly MailSender _mailSender;

        public MailSenderController(ILogger<MailSenderController> logger, MailSender mailSender)
        {
            _logger = logger;
            _mailSender = mailSender;
        }


        [HttpPost("{authToken}")]
        public async Task<ActionResult> SendResume(string authToken, [FromBody] SendResumeDto sendResumeDto)
        {
            if (String.IsNullOrEmpty(authToken))
            {
                return Unauthorized();
            }
            await _mailSender.SendResume(authToken, sendResumeDto.PositionId);
            return Ok();
        }
    }
}