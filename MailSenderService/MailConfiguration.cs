﻿namespace MailSenderService
{
    public class MailConfiguration
    {
        public string MailAddress { get; set; }
        public string Password { get; set; }
    }
}
